
Name: app-ibvpn
Epoch: 1
Version: 1.2.5
Release: 1%{dist}
Summary: ibVPN
License: GPLv3
Group: Applications/Apps
Packager: Darryl Sokoloski
Vendor: Darryl Sokoloski
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Invisible Browsing VPN.  ibVPN is a service that allows you to surf the Web invisibly and securely without leaving a trace and without being tracked.  By using an ibVPN server located in another country you can access geographically locked content and services such as Hulu, BBC Iplayer, Pandora, and Netflix.

%package core
Summary: ibVPN - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-base-core >= 1.1.5
Requires: app-network-core
Requires: app-dhcp-core
Requires: openvpn
Requires: csplugin-routewatch

%description core
Invisible Browsing VPN.  ibVPN is a service that allows you to surf the Web invisibly and securely without leaving a trace and without being tracked.  By using an ibVPN server located in another country you can access geographically locked content and services such as Hulu, BBC Iplayer, Pandora, and Netflix.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/ibvpn
cp -r * %{buildroot}/usr/clearos/apps/ibvpn/
rm -f %{buildroot}/usr/clearos/apps/ibvpn/README.md

install -d -m 0755 %{buildroot}/etc/clearos/firewall.d
install -d -m 0755 %{buildroot}/etc/clearos/ibvpn.d
install -d -m 0755 %{buildroot}/var/run/ibvpn
install -d -m 0750 %{buildroot}/var/state/webconfig
install -D -m 0755 packaging/10-ibvpn %{buildroot}/etc/clearos/firewall.d/
install -D -m 0644 packaging/ibvpn-daemon.php %{buildroot}/var/clearos/base/daemon/ibvpn.php
install -D -m 0755 packaging/ibvpn-exec-post.sh %{buildroot}/usr/libexec/ibvpn/ibvpn-exec-post.sh
install -D -m 0755 packaging/ibvpn-exec-pre.sh %{buildroot}/usr/libexec/ibvpn/ibvpn-exec-pre.sh
install -D -m 0644 packaging/ibvpn.com.crt %{buildroot}/etc/clearos/ibvpn.d/ibvpn.com.crt
install -D -m 0644 packaging/ibvpn.conf %{buildroot}/etc/clearos/ibvpn.conf
install -D -m 0644 packaging/ibvpn.service %{buildroot}/lib/systemd/system/ibvpn.service
install -D -m 0644 packaging/ibvpn.tmpf %{buildroot}/usr/lib/tmpfiles.d/ibvpn.conf
install -D -m 0755 packaging/route-down.sh %{buildroot}/etc/clearos/ibvpn.d/
install -D -m 0755 packaging/route-up.sh %{buildroot}/etc/clearos/ibvpn.d/
install -D -m 0644 packaging/routewatch-ibvpn.conf %{buildroot}/etc/clearsync.d/routewatch-ibvpn.conf

%post
logger -p local6.notice -t installer 'app-ibvpn - installing'

%post core
logger -p local6.notice -t installer 'app-ibvpn-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/ibvpn/deploy/install ] && /usr/clearos/apps/ibvpn/deploy/install
fi

[ -x /usr/clearos/apps/ibvpn/deploy/upgrade ] && /usr/clearos/apps/ibvpn/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ibvpn - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ibvpn-api - uninstalling'
    [ -x /usr/clearos/apps/ibvpn/deploy/uninstall ] && /usr/clearos/apps/ibvpn/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/ibvpn/controllers
/usr/clearos/apps/ibvpn/htdocs
/usr/clearos/apps/ibvpn/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/ibvpn/packaging
%exclude /usr/clearos/apps/ibvpn/unify.json
%dir /usr/clearos/apps/ibvpn
%dir /etc/clearos/firewall.d
%dir /etc/clearos/ibvpn.d
%dir /var/run/ibvpn
%dir %attr(0750,root,webconfig) /var/state/webconfig
/usr/clearos/apps/ibvpn/deploy
/usr/clearos/apps/ibvpn/language
/usr/clearos/apps/ibvpn/libraries
/etc/clearos/firewall.d/
/var/clearos/base/daemon/ibvpn.php
/usr/libexec/ibvpn/ibvpn-exec-post.sh
/usr/libexec/ibvpn/ibvpn-exec-pre.sh
/etc/clearos/ibvpn.d/ibvpn.com.crt
%config(noreplace) /etc/clearos/ibvpn.conf
/lib/systemd/system/ibvpn.service
/usr/lib/tmpfiles.d/ibvpn.conf
/etc/clearos/ibvpn.d/
/etc/clearos/ibvpn.d/
%config /etc/clearsync.d/routewatch-ibvpn.conf
