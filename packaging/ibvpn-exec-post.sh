#!/bin/sh

# Remove any ip rules
while true; do ip rule del pref 20 >/dev/null 2>&1 || break; done
ip route flush table 20 >/dev/null 2>&1

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4 syntax=sh
