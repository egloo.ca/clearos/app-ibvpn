#!/bin/sh

# OpenVPN configlet directory
ibvpn_confd="/etc/clearos/ibvpn.d"

# Location of ibvpn symlink to openvpn
ibvpn="/usr/sbin/ibvpn"
[ ! -h "$ibvpn" ] && ln -sf /usr/sbin/openvpn /usr/sbin/ibvpn

# PID directory
piddir="/var/run/ibvpn"

# Source configuration file
source /etc/clearos/ibvpn.conf

# Load tun module
/sbin/modprobe tun >/dev/null 2>&1

# Ensure we've been configured
if [ -z "$ibvpn_conf" -o ! -f "$ibvpn_confd/ibvpn.ovpn" ]; then
    echo "Can not start ibVPN, not configured."
    exit 1
fi

# Get WAN IP address
if [ "$external_interface" == "auto" ]; then
    source /etc/clearos/network.conf
    if [ -z "$EXTIF" ]; then
        echo "No external interfaces configured."
        exit 1
    fi
    for ifn in $EXTIF; do
        external_interface=$ifn
        break
    done
fi

wan_ipv4=$(ip -4 addr ls dev $external_interface | grep inet | sed -e 's/^.*inet \([0-9\.]*\).*$/\1/' | head -n 1)

# Set systemd environment variables
systemctl set-environment IBVPN_CONFIG="$ibvpn_confd/ibvpn.ovpn"
systemctl set-environment IBVPN_CD="$ibvpn_confd"
systemctl set-environment IBVPN_WANIP="$wan_ipv4"
systemctl set-environment IBVPN_PORT="$ibvpn_port"

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4 syntax=sh
