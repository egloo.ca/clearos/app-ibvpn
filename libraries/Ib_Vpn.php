<?php

/**
 * ibVPN class.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 Darryl Sokoloski
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ibvpn;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ?
    getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';

require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ibvpn');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Folder as Folder;
use \clearos\apps\base\Shell as Shell;
use \clearos\apps\base\Webconfig as Webconfig;
use \clearos\apps\network\Iface_Manager as Iface_Manager;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Folder');
clearos_load_library('base/Shell');
clearos_load_library('base/Webconfig');
clearos_load_library('network/Iface_Manager');

// Exceptions
//-----------

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;
use \clearos\apps\base\Folder_Not_Found_Exception as Folder_Not_Found_Exception;
use \clearos\apps\base\File_No_Match_Exception as File_No_Match_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/File_Not_Found_Exception');
clearos_load_library('base/Folder_Not_Found_Exception');
clearos_load_library('base/File_No_Match_Exception');
clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN class.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

class Ib_Vpn extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/clearos/ibvpn.conf';
    const FILE_ACCOUNT = '/etc/clearos/ibvpn.d/passwd';
    const FILE_SERVICE = '/etc/clearos/ibvpn.d/services.json';

    const FILE_OVPN_CONFIG = '/etc/clearos/ibvpn.d/ibvpn.ovpn';
    const FILE_OVPN_TEMPLATE = '/usr/clearos/apps/ibvpn/deploy/template.ovpn';

    const API_URL = 'https://api.ibvpn.net/aiowin/v3/';
    const API_KEY = '133040d4d03f4a5bc78434dd2901d5f4';

    const SCRIPT_ROUTE = '/etc/clearos/ibvpn.d/route-up.sh';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * ibVPN constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Set account credentials.
     *
     * @param string $username username (email address)
     * @param string $password password
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_account($username, $password)
    {
        $file = new File(self::FILE_ACCOUNT);
        if ($file->exists()) $file->delete();
        $file->create('root', 'root', '0600');
        $file->dump_contents_from_array(array($username, $password));
    }

    /**
     * Get account credentials.
     *
     * @return array array containing credentials
     * @throws Engine_Exception
     */

    public function get_account()
    {
        $file = new File(self::FILE_ACCOUNT);
        if (! $file->exists()) return false;
        $lines = $file->get_contents_as_array();
        if (count($lines) < 2) return false;
        $username = trim($lines[0]);
        $password = trim($lines[1]);
        if (strlen($username) == 0 || strlen($password) == 0)
            return false;
        return array('username' => $username, 'password' => $password);
    }

    /**
     * Set ibVPN server.
     *
     * @param string $server_name ibVPN server (configuration file)
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_server($server_name)
    {
        $addrs = array();
        $file = new File(self::FILE_SERVICE);
        if (! $file->exists()) return $servers;

        $json = json_decode($file->get_contents());

        foreach ($json->packages as $obj) {
            if (property_exists($obj, '999')) continue;
            if (! property_exists($obj, 'servers')) continue;

            foreach ($obj->servers as $server) {
                if (key(get_object_vars($server)) != $server_name) continue;

                $servers = get_object_vars($server);
                $addrs[] = $servers[$server_name][0];

                break 2;
            }
        }

        if (! count($addrs)) return;

        $file = new File(self::FILE_CONFIG);
        if (! $file->exists())
            $file->create('root', 'root', '0644');
        try {
            $file->replace_one_line(
                '/^ibvpn_conf/', "ibvpn_conf=\"$server_name\"\n");
        } catch (File_No_Match_Exception $e) {
            $file->add_lines("ibvpn_conf=\"$server_name\"\n");
        }

        $this->_generate_ovpn_config($server_name, $addrs);
    }

    /**
     * Generate the OpenVPN server configuration.
     *
     * @param string $name Chosen ibVPN server name
     * @param array $addrs Chosen ibVPN server addresses
     *
     * @return void
     * @throws Engine_Exception
     */

    private function _generate_ovpn_config($name, $addrs)
    {
        $file = new File(self::FILE_OVPN_TEMPLATE);
        $template = $file->get_contents_as_array();

        $config = array(
            "# OpenVPN Configuration for ibVPN server: $name",
            "# WARNING!  Dynamically generated.  Local modifications will be LOST.");
        foreach ($addrs as $addr) {
            $config[] = "remote $addr 1194 udp";
            $config[] = "remote $addr 80 udp";
            $config[] = "remote $addr 53 udp";
            $config[] = "remote $addr 443 udp";
        }

        $file = new File(self::FILE_OVPN_CONFIG);
        if (! $file->exists()) $file->create('root', 'root', '0644');
        $file->dump_contents_from_array(array_merge($config, array(''), $template));
    }

    /**
     * Get ibVPN server.
     *
     * @return string ibVPN server
     * @throws Engine_Exception
     */

    public function get_server()
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return false;
        try {
            $server = $file->lookup_line('/^ibvpn_conf/');
        } catch (File_No_Match_Exception $e) {
            return false;
        }

        $server = preg_replace('/ibvpn_conf\s*=\s*/', '', $server);
        return preg_replace('/"/', '', $server);
    }

    /**
     * Set external network interface.
     *
     * @param string $extnic External network interface.
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_external_interface($extnic)
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists())
            $file->create('root', 'root', '0644');
        try {
            $file->replace_one_line(
                '/^external_interface.*/', "external_interface=\"$extnic\"\n");
        } catch (File_No_Match_Exception $e) {
            $file->add_lines("external_interface=\"$extnic\"\n");
        }
    }

    /**
     * Get external network interface.
     *
     * @return string External network interface.
     * @throws Engine_Exception
     */

    public function get_external_interface()
    {
        $file = new File(self::FILE_CONFIG);
        $iface_manager = new Iface_Manager();
        if (! $file->exists()) return false;
        try {
            $extnic = $file->lookup_line('/^external_interface/');
        } catch (File_No_Match_Exception $e) {
            return $iface_manager->get_external_interface();
        }

        $extnic = preg_replace('/external_interface\s*=\s*/', '', $extnic);
        $extnic = preg_replace('/"/', '', $extnic);

        if (! strlen($extnic)) {
            $info = $iface_manager->get_external_interface();
            $extnic = $info['ifcfg']['device'];
        }

        return $extnic;
    }

    /**
     * Get list of all ibVPN servers.
     *
     * @return array array containing ibVPN servers
     * @throws Engine_Exception
     */

    public function get_server_list()
    {
        $servers = array();
        $file = new File(self::FILE_SERVICE);
        if (! $file->exists()) return $servers;

        $json = json_decode($file->get_contents());

        foreach ($json->packages as $obj) {
            if (property_exists($obj, '999')) continue;
            if (! property_exists($obj, 'servers')) continue;

            foreach ($obj->servers as $server)
                $servers[] = key(get_object_vars($server));
        }

        sort($servers, SORT_NATURAL);
        return $servers;
    }

    /**
     * Update ibVPN servers.
     *
     * @return string JSON encoded string containing update status.
     * @throws Engine_Exception
     */

    public function update_servers()
    {
        $userid = 0;
        $json = array(
            'success' => false,
            'status' => lang('ibvpn_status_none'));

        try {
            $account = $this->get_account();
            if ($account === false) {
                $json['status'] = lang('ibvpn_account_not_set');
                return json_encode($json);
            }
        }
        catch (Exception $e) {
            $json['status'] = $e->getMessage();
            return json_encode($json);
        }

        try {
            $params = array(
                'methodName' => 'login',
                'username' => $account['username'],
                'password' => $account['password'],
                'apikey' => self::API_KEY
            );
            $result = $this->_make_api_request($params);
        }
        catch (Exception $e) {
            $json['status'] = $e->getMessage();
            return json_encode($json);
        }

        if (! array_key_exists('userId', $result)) {
            $json['status'] = lang('ibvpn_status_unexpected_result');
            return json_encode($json);
        }

        $userid = $result['userId'];

        try {
            $params = array(
                'methodName' => 'getUserServices',
                'userId' => $userid,
                'password' => $account['password']
            );
            $result = $this->_make_api_request($params);
        }
        catch (Exception $e) {
            $json['status'] = $e->getMessage();
            return json_encode($json);
        }

        if (! array_key_exists('packages', $result)) {
            $json['status'] = lang('ibvpn_status_unexpected_result');
            return json_encode($json);
        }

        try {
            $file = new File(self::FILE_SERVICE, true);
            if (! $file->exists()) $file->create('root', 'webconfig', '0640');
            $file->dump_contents_from_array(
                explode("\n", json_encode($result, JSON_PRETTY_PRINT))
            );

            $json['success'] = true;
            $json['status'] = lang('ibvpn_status_update_success');
        }
        catch (Exception $e) {
            $json['status'] = lang('ibvpn_status_update_exception');
        }

        return json_encode($json);
    }

    /**
     * Make API call to ibVPN.
     *
     * @params array $params Form paramters
     *
     * @return string JSON result.
     * @throws Engine_Exception
     */

    private function _make_api_request($params)
    {
        $ch = curl_init(self::API_URL);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_REFERER, 'https://www.clearos.com/');
        curl_setopt($ch, CURLOPT_USERAGENT,
            'app-ibvpn/1.2.3 (+https://www.clearos.com/)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        // Check for upstream proxy settings
        //----------------------------------

        if (clearos_app_installed('upstream_proxy')) {
            clearos_load_library('upstream_proxy/Proxy');

            $proxy = new \clearos\apps\upstream_proxy\Proxy();

            $proxy_server = $proxy->get_server();
            $proxy_port = $proxy->get_port();
            $proxy_username = $proxy->get_username();
            $proxy_password = $proxy->get_password();

            if (! empty($proxy_server))
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);

            if (! empty($proxy_port))
                curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);

            if (! empty($proxy_username)) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD,
                    $proxy_username . ':' . $proxy_password);
            }
        }

        // Send POST request
        //------------------

        $result = curl_exec($ch);

        // Validate response
        //------------------

        if ($result === false)
            throw new Engine_Exception(curl_error($ch));

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 ||
            curl_getinfo($ch, CURLINFO_CONTENT_TYPE) != 'application/json')
            throw new Engine_Exception(lang('ibvpn_status_unexpected_result'));

        if (($json = json_decode($result, true)) === false)
            throw new Engine_Exception(lang('ibvpn_status_unexpected_result'));

        if (! array_key_exists('errorcode', $json))
            throw new Engine_Exception(lang('ibvpn_status_unexpected_result'));

        if ($json['errorcode'] != 0) {
            $message = sprintf('%s [%d]',
                array_key_exists('message', $json) ?
                    $json['message'] : lang('ibvpn_status_unexpected_result'),
                $json['errorcode']);
            throw new Engine_Exception($message);
        }

        return $json;
    }

    /**
     * Add device (by IP address) to route through ibVPN.
     *
     * @param string $ip IP address of device
     *
     * @return boolean false if configuration doesn't exist.
     * @throws Engine_Exception
     */

    public function add_device($ip)
    {
        $file = new File(self::FILE_CONFIG, true);
        if (! $file->exists()) return false;
        $devices = $this->get_device_list();
        if (array_search($ip, $devices) === false) {
            $devices[] = $ip;
            $lines = '';
            foreach ($devices as $key => $device)
                $lines .= "ibvpn_device[$key]=\"$device\"\n";
            $file->delete_lines('/^ibvpn_device/');
            $file->add_lines($lines);

            try {
                $shell = new Shell();
                $shell->execute(self::SCRIPT_ROUTE, '', true);
            } catch (Exception $e) { }

            return true;
        }

        return false;
    }

    /**
     * Delete device (by IP address) from ibVPN routing rules.
     *
     * @param string $ip IP address of device to delete
     *
     * @return boolean false if configuration doesn't exist.
     * @throws Engine_Exception
     */

    public function delete_device($ip)
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return false;
        $devices = $this->get_device_list();
        if (($key = array_search($ip, $devices)) !== false) {
            unset($devices[$key]);
            sort($devices);
            $lines = '';
            foreach ($devices as $key => $device)
                $lines .= "ibvpn_device[$key]=\"$device\"\n";
            $file->delete_lines('/^ibvpn_device/');
            $file->add_lines($lines);

            try {
                $shell = new Shell();
                $shell->execute(self::SCRIPT_ROUTE, '', true);
            } catch (Exception $e) { }

            return true;
        }

        return false;
    }

    /**
     * Get list of all devices configured to be routed through ibVPN.
     *
     * @return array array containing devices routed through ibVPN
     * @throws Engine_Exception
     */

    public function get_device_list()
    {
        $devices = array();
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return $devices;
        $lines = $file->get_contents_as_array();
        foreach ($lines as $line) {
            if (! preg_match(
                '/^ibvpn_device\[(\d+)\]\s*=\s*"(.*)"/', $line, $matches)) continue;
            $devices[] = $matches[2];
        }

        sort($devices);

        return $devices;
    }

    public function sync_routing_rules()
    {
        try {
            $shell = new Shell();
            $shell->execute(self::SCRIPT_ROUTE, '', true);
        } catch (Exception $e) { }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a hostname or IP address.
     *
     * @param string $addr Address
     *
     * @return string error message if address is invalid
     */

    public function validate_address($addr)
    {
        clearos_profile(__METHOD__, __LINE__);

        $ip = gethostbyname($addr);
        if (inet_pton($ip) !== false) return '';

        return lang('ibvpn_address_invalid');
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
