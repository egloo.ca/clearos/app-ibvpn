<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'ibvpn';
$app['version'] = '1.2.5';
$app['release'] = '1';
$app['vendor'] = 'Darryl Sokoloski';
$app['packager'] = 'Darryl Sokoloski';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['summary'] = lang('ibvpn_ibvpn');
$app['description'] = lang('ibvpn_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('ibvpn_ibvpn');
$app['category'] = lang('base_category_network');
$app['subcategory'] = lang('ibvpn_subcategory');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-base-core >= 1.1.5',
    'app-network-core',
    'app-dhcp-core',
    'openvpn',
    'csplugin-routewatch',
);

$app['core_directory_manifest'] = array(
    '/etc/clearos/firewall.d' => array(),
    '/etc/clearos/ibvpn.d' => array(),
    '/var/run/ibvpn' => array(),
    '/var/state/webconfig' => array(
    'owner' => 'root',
    'group' => 'webconfig',
    'mode' => '0750'),
);

$app['core_file_manifest'] = array(
    '10-ibvpn' => array(
        'target' => '/etc/clearos/firewall.d/',
        'mode' => '0755',
    ),
    'ibvpn.conf' => array(
        'target' => '/etc/clearos/ibvpn.conf',
        'config' => TRUE,
        'config_params' => 'noreplace',
    ),
    'ibvpn.service' => array(
        'target' => '/lib/systemd/system/ibvpn.service',
        'mode' => '0644',
    ),
    'ibvpn-exec-pre.sh' => array(
        'target' => '/usr/libexec/ibvpn/ibvpn-exec-pre.sh',
        'mode' => '0755',
    ),
    'ibvpn-exec-post.sh' => array(
        'target' => '/usr/libexec/ibvpn/ibvpn-exec-post.sh',
        'mode' => '0755',
    ),
    'route-up.sh' => array(
        'target' => '/etc/clearos/ibvpn.d/',
        'mode' => '0755',
    ),
    'route-down.sh' => array(
        'target' => '/etc/clearos/ibvpn.d/',
        'mode' => '0755',
    ),
    'ibvpn.com.crt' => array(
        'target' => '/etc/clearos/ibvpn.d/ibvpn.com.crt',
        'mode' => '0644',
    ),
    'ibvpn-daemon.php' => array(
        'target' => '/var/clearos/base/daemon/ibvpn.php',
        'mode' => '0644',
    ),
    'ibvpn.tmpf' => array(
        'target' => '/usr/lib/tmpfiles.d/ibvpn.conf',
        'mode' => '0644',
        'owner' => 'root',
        'group' => 'root',
    ),
    'routewatch-ibvpn.conf' => array(
        'target' => '/etc/clearsync.d/routewatch-ibvpn.conf',
        'mode' => '0644',
        'owner' => 'root',
        'group' => 'root',
        'config' => TRUE,
    ),
);

$app['delete_dependency'] = array(
    'app-ibvpn-core',
);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
