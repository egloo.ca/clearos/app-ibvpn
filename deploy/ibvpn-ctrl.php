#!/usr/clearos/sandbox/usr/bin/php
<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// E N A B L E  /  D I S A B L E  I B V P N
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\ibvpn\Ib_Vpn as Ib_Vpn;
use \clearos\apps\base\Daemon as Daemon;

clearos_load_library('base/Daemon');
clearos_load_library('ibvpn/Ib_Vpn');

class Ib_Vpn_Daemon extends Daemon
{
    function __construct()
    {
        parent::__construct('ibvpn', 'ibvpn');
    }
}

$mode = NULL;
$server = NULL;
$ip = NULL;

if ($argc < 3) {
    echo "Required argument missing.\n";
    exit(1);
}

if ($argv[1] == '--enable')
    $mode = 1;
else if ($argv[1] == '--disable')
    $mode = 2;
else {
    echo "Unexpected argument: {$argv[1]}\n";
    exit(1);
}

if ($mode == 1 && $argc != 4) {
    echo "Required argument missing.\n";
    exit(1);
}

if ($mode == 1) $server = $argv[3];
$ip = $argv[2];

$ibvpn = new Ib_Vpn();
$ibvpn_daemon = new Ib_Vpn_Daemon();

switch ($mode) {
case 1:
    $servers = $ibvpn->get_server_list();
    $current_server = $ibvpn->get_server();
    if (array_search($server, $servers) === FALSE) {
        printf("Server not found: %s\n", $server);
        exit(1);
    }
    if ($server != $current_server)
        $ibvpn->set_server($server);

    $devices = $ibvpn->get_device_list();
    if (array_search($ip, $devices) === FALSE)
        $ibvpn->add_device($ip);

    if ($server != $current_server ||
        $ibvpn_daemon->get_running_state() === FALSE)
        $ibvpn_daemon->restart();
    else
        $ibvpn->sync_routing_rules();

    break;
case 2:
    $ibvpn->delete_device($ip);
    break;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
