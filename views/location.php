<?php

/**
 * ibVPN server configuration.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage views
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 Darryl Sokoloski
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/date/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ibvpn');

///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

if ($form_type == 'edit') {
    $read_only = FALSE;
    $buttons = array( 
        form_submit_update('submit-form'),
        anchor_cancel('/app/ibvpn')
    );
} else {
    $read_only = TRUE;
    $buttons = array( 
        anchor_edit('/app/ibvpn/location/edit'),
        anchor_javascript('update', lang('ibvpn_synchronize'), 'high')
    );
}

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo form_open('ibvpn/location/edit', array('id' => 'server_form'));
echo form_header(lang('ibvpn_server_location'));
if ($read_only === FALSE || !strlen($server))
    echo form_banner("<p>" . lang('ibvpn_server_desc') . '</p>');
if ($read_only === FALSE || strlen($server))
    echo field_dropdown('server', $servers, $server, lang('ibvpn_server'), $read_only);
if ($read_only === FALSE) {
    if (count($extnics) == 1)
        echo "<input id='extnic' name='extnic' value='$extnic' type='hidden'>\n";
    else
        echo field_dropdown('extnic', $extnics, $extnic, lang('ibvpn_external_interface'), $read_only);
}

echo field_button_set($buttons);

echo form_footer();
echo form_close();

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
