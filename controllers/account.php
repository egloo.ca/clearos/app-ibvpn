<?php

/**
 * ibVPN Account Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN Account Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

class Account extends ClearOS_Controller
{
    /**
     * Index.
     */

    function index()
    {
        $this->view();
    }

    /**
     * Account edit view.
     *
     * @return view
     */

    function edit()
    {
        $this->_view_edit('edit');
    }

    /**
     * Account read-only view.
     *
     * @return view
     */

    function view()
    {
        $this->_view_edit('view');
    }

    /**
     * Account default controller
     *
     * @param string $form_type form type
     *
     * @return view
     */

    function _view_edit($form_type)
    {
        // Load libraries
        //---------------

        $this->load->library('ibvpn/Ib_Vpn');

        // Set validation rules
        //---------------------
        
        // TODO: Add validation rules 
        //$this->form_validation->set_policy('time_zone', 'date/Time', 'validate_time_zone', TRUE);
        //$this->form_validation->set_policy('auto_synchronize', 'date/NTP_Time', 'validate_state', TRUE);
        //$form_ok = $this->form_validation->run();
        $form_ok = TRUE;

        // Handle form submit
        //-------------------

        if (($this->input->post('username') && $form_ok)) {
            try {
                $this->ib_vpn->set_account(
                    $this->input->post('username'),
                    $this->input->post('password'));
                redirect('/ibvpn');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load view data
        //---------------

        $account = $this->ib_vpn->get_account();

        if ($account !== FALSE) {
            $data['username'] = $account['username'];
            $data['password'] = $account['password'];
        }

        $data['form_type'] = $form_type;

        $this->page->view_form('ibvpn/account', $data, lang('ibvpn_account'));
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
