<?php

/**
 * ibVPN Devices Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN Devices Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

class Devices extends ClearOS_Controller
{
    /**
     * Index.
     */

    function index()
    {
        // Load libraries
        //---------------

        //$this->load->library('dhcp/Dnsmasq');
        $this->load->library('ibvpn/Ib_Vpn');

        // Load view data
        //---------------

        $devices = $this->ib_vpn->get_device_list();
        if (! count($devices)) {
            $this->add(NULL);
            return;
        }

        foreach ($devices as $device) {
            $entry = array();
            $entry['device'] = gethostbyaddr($device);
            $entry['address'] = $device;
            $data['devices'][] = $entry;
        }

        $this->page->view_form('ibvpn/devices', $data, lang('ibvpn_devices'));
    }

    /**
     * Add device view.
     *
     * @return view
     */

    function add($device)
    {
        // Load libraries
        //---------------

        $this->load->library('ibvpn/Ib_Vpn');
        $this->load->library('dhcp/Dnsmasq');

        // Set validation rules
        //---------------------
       
        if (($this->input->post('hostname'))) {
            $this->form_validation->set_policy('hostname', 'ibvpn/Ib_Vpn', 'validate_address');
            $form_ok = $this->form_validation->run();
        }
        else if (strlen($device)) {
            $form_ok = TRUE;
        }

        // Handle form submit
        //-------------------
        if ($form_ok) {
            $ip = NULL;
            if (($this->input->post('hostname')))
                $ip = gethostbyname($this->input->post('hostname'));
            else if (strlen($device))
                $ip = $device;

            if (inet_pton($ip) === FALSE) $ip = NULL;

            if ($ip !== NULL) {
                try {
                    $this->ib_vpn->add_device($ip);
                    redirect('/ibvpn');
                } catch (Exception $e) {
                    $this->page->view_exception($e);
                    return;
                }
            }
        }

        // Load view data
        //---------------

        $devices = $this->ib_vpn->get_device_list();
        $leases = $this->dnsmasq->get_leases();

        foreach ($leases as $lease) {
            if (! strlen($lease['ip']) ||
                array_search($lease['ip'], $devices) !== FALSE)
                continue;

            if (! strlen($lease['vendor']))
                $lease['vendor'] = lang('ibvpn_device_unknown');
            if (! strlen($lease['hostname']))
                $lease['hostname'] = lang('ibvpn_device_unknown');

            $data['devices'][$lease['ip']]['ip'] = $lease['ip'];
            $data['devices'][$lease['ip']]['vendor'] = $lease['vendor'];
            $data['devices'][$lease['ip']]['hostname'] = $lease['hostname'];
        }

        $this->page->view_form('ibvpn/device_add', $data, lang('ibvpn_device_add'));
    }

    /**
     * Delete device.
     *
     * @return view
     */

    function delete($device)
    {
        // Load libraries
        //---------------

        $this->load->library('ibvpn/Ib_Vpn');

        try {
            $this->ib_vpn->delete_device($device);
        } catch (Exception $e) { }

        redirect('/ibvpn');
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
