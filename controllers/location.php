<?php

/**
 * ibVPN Location Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN Location Controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

class Location extends ClearOS_Controller
{
    /**
     * Index.
     */

    function index()
    {
        $this->_view_edit('view');
    }

    /**
     * Account edit view.
     *
     * @return view
     */

    function edit()
    {
        $this->_view_edit('edit');
    }

    /**
     * Account default controller
     *
     * @param string $form_type form type
     *
     * @return view
     */

    function _view_edit($form_type)
    {
        // Load libraries
        //---------------

        $this->load->library('ibvpn/Ib_Vpn');
        $this->load->library('network/Iface_Manager');

        $data['servers'] = $this->ib_vpn->get_server_list();
        $data['extnics'] = $this->iface_manager->get_external_interfaces();

        // Handle form submit
        //-------------------
        if ($this->input->post('server')) {
            try {
                $this->ib_vpn->set_server(
                    $data['servers'][$this->input->post('server')]);
                if (count($data['extnics']) == 1) {
                    $this->ib_vpn->set_external_interface(
                        $this->input->post('extnic'));
                }
                else {
                    $this->ib_vpn->set_external_interface(
                        $data['extnics'][$this->input->post('extnic')]);
                }
                redirect('/ibvpn');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load view data
        //---------------

        $server = $this->ib_vpn->get_server();
        if ($server !== FALSE &&
            ($server = array_search($server, $data['servers'])) !== FALSE)
            $data['server'] = $server;
        $data['extnic'] = $this->ib_vpn->get_external_interface();

        $data['form_type'] = $form_type;
        $this->page->view_form('ibvpn/location', $data, lang('ibvpn_server'));
    }

    /**
     * Update servers.
     */

    function update()
    {
        // Load dependencies
        //------------------

        $this->load->library('ibvpn/Ib_Vpn');

        // Run server update
        //-------------------

        try {
            $data = $this->ib_vpn->update_servers();
        } catch (Exception $e) {
            $data['success'] = FALSE;
            $data['status'] = clearos_exception_message($e);
        }

        // Return status message
        //----------------------

        $this->output->set_header("Content-Type: application/json");
        $this->output->set_output(json_encode($data));
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
